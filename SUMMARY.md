# Summary

<!-- Readme included as Introduction by default -->

<!-- * [Content Plan](content-plan.md) -->
* [Requirements](requirements.md)
    <!-- * [Additional Resources](additional-resources.md) -->
    <!-- * [Theory: Clojure Overview](overview/index.md) -->
* [Overview](clojure-web-services-overview.md)

## Clojure CLI Projects
* [Status Monitor](projects/status-monitor-deps/index.md)
    * [Application server](projects/status-monitor-deps/application-server.md)
    * [Debug requests](projects/status-monitor-deps/debugging-requests.md)
    * [Unit Test & Mocking](projects/status-monitor-deps/unit-test-mocking-handlers.md)
    * [Defining handlers](projects/status-monitor-deps/refactor-handlers-and-tests.md)
    * [Continuous integration](projects/status-monitor-deps/continuous-integration.md)
    * [Deployment via CI](projects/status-monitor-deps/deployment-via-ci.md)

* [Banking on Clojure](projects/banking-on-clojure/index.md)
    * [App Server Configuration](projects/banking-on-clojure/application-server-configuration.md)
    * [Refactor handlers](projects/banking-on-clojure/refactor-handler.md)
    * [Defining handlers](projects/banking-on-clojure/defining-handlers.md)
    * [Continuous Integration](projects/banking-on-clojure/continuous-integration.md)
    * [Deployment Pipeline](projects/banking-on-clojure/deployment-pipeline.md)
    * [Deployment via CI to Heroku](projects/banking-on-clojure/deployment-via-ci.md)
    * [Unit Tests](projects/banking-on-clojure/unit-tests.md)
    * [Clojure Specs](projects/banking-on-clojure/spec-generative-testing.md)
    * [Development database](projects/banking-on-clojure/development-database.md)
    * [Instrument next.jdbc functions](projects/banking-on-clojure/instrument-next-jdbc-functions.md)
    * [Database tables](projects/banking-on-clojure/database-tables.md)
    * [Database queries](projects/banking-on-clojure/database-queries.md)
        * [Create Records](projects/banking-on-clojure/create-records.md)
        * [Read Records](projects/banking-on-clojure/read-records.md)
        * [Update Records](projects/banking-on-clojure/update-records.md)
        * [Delete Records](projects/banking-on-clojure/delete-records.md)
    * [Cyclic Load Dependency](projects/banking-on-clojure/cyclic-load-dependency.md)
    * [Spec: generate database data](projects/banking-on-clojure/clojure-spec-generate-mock-data.md)
    * [Unit Testing Database](projects/banking-on-clojure/unit-testing-the-database.md)
    <!-- * [Migratus](projects/banking-on-clojure/migratus.md) -->
    * [HoneySQL](projects/banking-on-clojure/honeysql.md)
    * [Namespace design](projects/banking-on-clojure/namespace-design.md)
    * [Production database](projects/banking-on-clojure/production-database.md)
<!-- TODO: Write Game Scoreboard API with reitit -->
<!-- * [Game Scoreboard API](projects/game-scoreboard-api/index.md) -->

## REPL Driven Development

* [Overview](repl-driven-development/index.md)
* [integrant REPL](repl-driven-development/integrant-repl/index.md)
<!-- * [Aero Environment Profiles](repl-driven-development/aero-environment-profiles.md) -->

## Libraries

* [Reitit](libraries/reitit/index.md)
    * [Routing](libraries/reitit/constructing-routes.md)



## Architectural Components
* [Application servers](app-servers/index.md)
    * [Overview](app-servers/overview.md)
    * [Clojure project](app-servers/clojure-project.md)
    * [Create server](app-servers/create-server.md)
        * [Jetty server options](app-servers/jetty-server-options.md)
        * [Httpkit server options](app-servers/http-kit-server-options.md)
    * [Route requests](app-servers/route-requests.md)
    * [Start Server](app-servers/start-server.md)
        * [Java Properties](app-servers/java-system-properties.md)
    * [Simple restart](app-servers/simple-restart.md)
    * [Atom based restart](app-servers/atom-based-restart.md)
    <!-- * [Ring wrap-reload](app-servers/ring-wrap-reload.md) -->
    <!-- * [Component lifecycle](app-servers/component-lifecycle/index.md) -->
    <!--     * [mount](app-servers/component-lifecycle/mount.md) -->
    <!--     * [component](app-servers/component-lifecycle/component.md) -->
    <!--     * [integrant](app-servers/component-lifecycle/integrant.md) -->
    * [Logging](app-servers/app-server-logging.md)

<!-- * [Application logic](application-logic/index.md) -->
<!--     * [Routing](application-logic/routing.md) -->
    <!-- * [Requests](application-logic/requests/index.md) -->
    <!-- * [Responses](application-logic/responses/index.md) -->
    <!-- * [handlers](application-logic/handlers/index.md) -->
    <!-- * [middleware](application-logic/middleware/index.md) -->
    <!-- * [Serving static content](app-servers/static-content.md) -->


* [Relational Database & SQL](relational-databases-and-sql/index.md)
    * [next.jdbc library](relational-databases-and-sql/next-jdbc-library/index.md)
        * [Component lifecycle](relational-databases-and-sql/next-jdbc-library/connection-pool-lifecycle.md)
    * [H2 Database](relational-databases-and-sql/h2-database/index.md)
        * [H2 schema design](relational-databases-and-sql/h2-database/schema-design.md)
        * [H2 database tools](relational-databases-and-sql/h2-database/database-tools.md)
    * [PostgreSQL Database](relational-databases-and-sql/postgresql-database.md)
        * [Managing Connections](relational-databases-and-sql/managing-connections.md)


<!-- * [Specifications](specifications/index.md) -->
<!--     * [clojure.spec](specifications/clojure-spec/index.md) -->
<!--     * [Malli](specifications/malli/index.md) -->
    * [Function Specifications](specifications/functions.md)
    * [Relational Specifications](specifications/relational-specifications.md)
    * [Generative Testing](specifications/generative-testing.md)

<!-- * [Clojure databases](clojure-databases/index.md) -->
<!--     * [crux](clojure-databases/crux/index.md) -->

<!-- * [Key Value Store](key-value-store/index.md) -->
<!--     * [Redis](key-value-store/redis.md) -->


## Leiningen Projects

* [TODO WebApp](projects/leiningen/todo-app/index.md)
    * [Create a Project](projects/leiningen/todo-app/create-a-project/index.md)
        * [Update Project details](projects/leiningen/todo-app/create-a-project/update-project-details.md)
        <!-- * [Code so far](projects/leiningen/todo-app/create-a-project/code-so-far.md) -->
    * [Create a webserver with Ring](projects/leiningen/todo-app/create-a-webserver-with-ring/index.md)
        * [Add Ring Dependency](projects/leiningen/todo-app/create-a-webserver-with-ring/add-ring-dependency.md)
        * [Configure main namespace](projects/leiningen/todo-app/create-a-webserver-with-ring/configure-main-namespace.md)
        * [Theory: namespaces](projects/leiningen/todo-app/create-a-webserver-with-ring/namespaces.md)
        * [Include Ring Library](projects/leiningen/todo-app/create-a-webserver-with-ring/include-ring-library.md)
        * [Add a Jetty webserver](projects/leiningen/todo-app/create-a-webserver-with-ring/add-a-jetty-webserver.md)
        * [Theory: Coercing Types & java.lang](projects/leiningen/todo-app/create-a-webserver-with-ring/coersing-types-and-java-lang.md)
        * [Run webserver](projects/leiningen/todo-app/create-a-webserver-with-ring/run-webserver.md)
        <!-- * [Code so far](projects/leiningen/todo-app/create-a-webserver-with-ring/code-so-far.md) -->
    * [Theory: Introducing Ring](projects/leiningen/todo-app/introducing-ring/index.md)
    * [Create a handler function](projects/leiningen/todo-app/create-a-handler-function/index.md)
        * [Add error for bad routes](projects/leiningen/todo-app/create-a-handler-function/add-not-found.md)
        * [Theory: if function](projects/leiningen/todo-app/create-a-handler-function/if-function.md)
        * [Theory: maps and keywords](projects/leiningen/todo-app/create-a-handler-function/maps-and-keywords.md)
        <!-- * [Code so far](projects/leiningen/todo-app/create-a-handler-function/code-so-far.md) -->
    * [Unit test handler function](projects/leiningen/todo-app/unit-test-handler-function/index.md)
    * [Reloading the application](projects/leiningen/todo-app/reloading-the-application/index.md)
        * [Test your code reloads](projects/leiningen/todo-app/reloading-the-application/test-your-code-reloads.md)
        * [Middleware in Ring](projects/leiningen/todo-app/reloading-the-application/middleware.md)
        <!-- * [Code so far](projects/leiningen/todo-app/reloading-the-application/code-so-far.md) -->
    * [Compojure](projects/leiningen/todo-app/compojure/index.md)
        * [Theory: routing](projects/leiningen/todo-app/compojure/theory-routing.md)
        * [Adding dependency](projects/leiningen/todo-app/compojure/adding-dependency.md)
        * [Using Compojure](projects/leiningen/todo-app/compojure/using-compojure.md)
        * [Adding goodbye route](projects/leiningen/todo-app/compojure/adding-goodbye-route.md)
        * [About route](projects/leiningen/todo-app/compojure/about.md)
        * [Show request info](projects/leiningen/todo-app/compojure/show-request-info.md)
        * [Variable Path Elements](projects/leiningen/todo-app/compojure/variable-path-elements.md)
        * [Theory: local name bindings](projects/leiningen/todo-app/compojure/theory-local-name-bindings.md)
        * [Theory: using Clojure hash-maps](projects/leiningen/todo-app/compojure/theory-using-hash-maps.md)
        * [Lisp Calculator](projects/leiningen/todo-app/compojure/lisp-calculator.md)
        <!-- * [Code so far](projects/leiningen/todo-app/compojure/code-so-far.md) -->
    * [Deploying to Heroku](projects/leiningen/todo-app/heroku/index.md)
        * [Update the project](projects/leiningen/todo-app/heroku/update-project.md)
        * [Add Procfile](projects/leiningen/todo-app/heroku/procfile.md)
        * [Deploy to Heroku](projects/leiningen/todo-app/heroku/deploy.md)
        <!-- * [Code so far](projects/leiningen/todo-app/heroku/code-so-far.md) -->
    * [Hiccup HTML library](projects/leiningen/todo-app/hiccup/index.md)
        * [Updating handlers with hiccup](projects/leiningen/todo-app/hiccup/updating-handlers-with-hiccup.md)
        * [Create a new handler](projects/leiningen/todo-app/hiccup/create-new-handler.md)
        <!-- * [Code so far](projects/leiningen/todo-app/hiccup/code-so-far.md) -->
    * [Refactor namespace](projects/leiningen/todo-app/refactor-namespace/index.md)
        * [Base routes](projects/leiningen/todo-app/refactor-namespace/base-routes.md)
        <!-- * [Play routes](projects/leiningen/todo-app/refactor-namespace/play-routes.md) -->
        <!-- * [Task routes](projects/leiningen/todo-app/refactor-namespace/task-routes.md) -->
        * [Refactored Core](projects/leiningen/todo-app/refactor-namespace/core.md)
        <!-- * [Code so far](projects/leiningen/todo-app/refactor-namespace/code-so-far.md) -->
    * [Postgres Database](projects/leiningen/todo-app/postgres/index.md)
        * [Postgres install](projects/leiningen/todo-app/postgres/install.md)
        * [Environment Variables](projects/leiningen/todo-app/postgres/environment-variables.md)
        * [Postgres CLI](projects/leiningen/todo-app/postgres/postgres-cli.md)
        <!-- * [pgAdmin](projects/leiningen/todo-app/postgres/pg-admin.md) -->
        * [Dataclips](projects/leiningen/todo-app/postgres/dataclips.md)
        * [Connect to Postgres](projects/leiningen/todo-app/connect-to-postgres/index.md)
            * [Add Database Dependencies](projects/leiningen/todo-app/connect-to-postgres/add-database-dependencies.md)
            * [Define Database Connection](projects/leiningen/todo-app/connect-to-postgres/define-db-connection.md)
        * [Creating a database model](projects/leiningen/todo-app/database-model/index.md)
            * [Create table](projects/leiningen/todo-app/database-model/create-table.md)
            * [Create task](projects/leiningen/todo-app/database-model/create-task.md)
            * [List tasks](projects/leiningen/todo-app/database-model/show-all-task.md)
            * [Delete task](projects/leiningen/todo-app/database-model/delete-task.md)
            * [Alternative approaches](projects/leiningen/todo-app/database-model/alternative-approaches.md)
        * [Task handlers](projects/leiningen/todo-app/task-handlers/index.md)
            * [Add a task](projects/leiningen/todo-app/task-handlers/add-a-task.md)
            * [Show tasks](projects/leiningen/todo-app/task-handlers/show-task.md)
            * [Delete a task](projects/leiningen/todo-app/task-handlers/delete-a-task.md)
    <!-- * [A working example](projects/leiningen/todo-app/working-example/index.md) -->

## Building API's
* [Server-side API](server-side-api/index.md)
    * [Compojure-API template](server-side-api/compojure-api-template.md)
    * [Create API project](server-side-api/create-compojure-api-project.md)
    * [Swagger](server-side-api/swagger.md)
        * [ring-swagger](server-side-api/ring-swagger.md)
    * [Plumatic Schema](server-side-api/plumatic-schema.md)
    * [JSON files](server-side-api/json-files.md)
    * [Testing an API](server-side-api/testing-api.md)
        * [ring-mock library](server-side-api/ring-mock.md)
        * [cheshire library](server-side-api/cheshire.md)
    * [End to End Testing](server-side-api/end-to-end-testing/index.md)
        * [Swagger](server-side-api/end-to-end-testing/swagger.md)
        * [Curl](server-side-api/end-to-end-testing/curl.md)
        * [Insomnia](server-side-api/end-to-end-testing/insomnia.md)
        * [Postman Client](server-side-api/end-to-end-testing/postman.md)
    * [Terminology](server-side-api/terminology.md)
<!-- * [Project: Game Scoreboard](server-side-api/projects/game-scoreboard/index.md) -->
<!--     * [Defining scoreboard](server-side-api/projects/game-scoreboard/defining-scoreboard.md) -->
<!--     * [Defining Players](server-side-api/projects/game-scoreboard/defining-scores.md) -->
<!--     * [Game Scoreboard UI](server-side-api/projects/game-scoreboard-ui/index.md) -->

<!-- ## Micro-frameworks (TODO) -->

<!-- * [Overview](micro-framework/index.md) -->
<!--     * [Luminus](micro-framework/luminus/index.md) -->
<!--     * [Pedestal](micro-framework/pedestal/index.md) -->
<!--     * [Edge](micro-framework/edge/index.md) -->


## Reference
* [Ring](reference/ring/index.md)
    * [Request map](reference/ring/request-map.md)
<!-- * [Compojure defroutes macro](compojure/defroutes.md) -->


## Hacking on content
<!-- * [Variable tag names](work-in-progress.md) -->
    <!-- * [Using Postgres from Clojure](using-postgres/index.md) -->
* [Project: URL Shortner as a Service](project-url-shortner/index.md)
    * [Create project](project-url-shortner/create-project.md)
    * [Run project](project-url-shortner/run-project.md)
    * [Test app reloading](project-url-shortner/test-app-reloading.md)
    * [Compojure Template](project-url-shortner/compojure-template.md)
    * [Design data structure](project-url-shortner/design-data-structure.md)
    * [Whats in a request](project-url-shortner/whats-in-a-request.md)
    * [Redirect to full URL](project-url-shortner/redirect-to-full-url.md)
    <!--     * [Add static resources](project-url-shortner/add-static-resources.md) -->
    <!--     * [Disable anti-forgery check](project-url-shortner/disable-anti-forgery-check.    md) -->
    <!--     * [Create HTML Form](project-url-shortner/create-html-form.md) -->
    <!--     * [Using Ring Redirect](project-url-shortner/using-ring-redirect.md) -->
    <!--     * [Named alias handler](project-url-shortner/named-alias-handler.md) -->
    <!--     * [if-let function](project-url-shortner/if-let-function.md) -->
    <!--     * [Refactor: Hiccup form](project-url-shortner/refacor-hiccup-form.md) -->
    <!--     * [Alias generator](project-url-shortner/alias-generator.md) -->
    <!--     * [Persist aliases](project-url-shortner/persist-aliases.md) -->
    <!--     * [Postgres setup](project-url-shortner/postgres-setup.md) -->
    <!--     * [Redis setup](project-url-shortner/redis-setup.md) -->
    <!--     * [create database](project-url-shortner/create-database.md) -->
    <!--     * [add alias to database](project-url-shortner/add-alias-to-database.md) -->
    <!--     * [get alias from database](project-url-shortner/get-alias-from-database.md) -->
    <!--     * [delete alias from database](project-url-shortner/delete-alias-from-database.md) -->
